(function() {
    'use strict';

    const url =  window.location.href;


    const exists = document.querySelector("#simplify");

    // Short circuit
    if (exists) return;

    const style = document.createElement("style");
    style.id = "simplify"
    style.innerHTML = `
    #metadata-line.ytd-video-meta-block span.ytd-video-meta-block,
    ytd-toggle-button-renderer.style-text[is-icon-button] #text.ytd-toggle-button-renderer,
    ytd-video-primary-info-renderer[has-date-text] #info-text.ytd-video-primary-info-renderer,
    #owner-sub-count.ytd-video-owner-renderer,
    #vote-count-middle.ytd-comment-action-buttons-renderer,
    .count-text.ytd-comments-header-renderer, #metadata-line.ytd-grid-video-renderer span.ytd-grid-video-renderer,
    ytd-toggle-button-renderer.force-icon-button a.ytd-toggle-button-renderer
    {
        display: none;
    }
    `;
    document.head.appendChild(style);
})();